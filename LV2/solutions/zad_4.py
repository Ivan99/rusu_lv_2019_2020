#!/usr/bin/env python
# coding: utf-8

# In[4]:


import numpy as np 
import matplotlib.pyplot as plt

br1 = 0;
br2 = 6;
niz = np.random.randint(1, 7, size = 100)

uz = range(min(niz), max(niz)+2)
plt.hist(niz, uz, color = "blue")
plt.grid(linestyle = '--')


import scipy as sp
from sklearn import cluster, datasets
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import matplotlib.image as mpimg 


try:
    img = sp.img(gray=True)
except AttributeError:
    from scipy import misc
    img = mpimg.imread('LV5\\resources\\example.png') 
X = img.reshape((-1, 1))
k_means = cluster.KMeans(n_clusters=10, n_init=1)
k_means.fit(X)
values = k_means.cluster_centers_.squeeze()
labels = k_means.labels_
face_compressed = np.choose(labels, values)
face_compressed.shape = img.shape

plt.figure()
plt.imshow(img ,  cmap="gray")

plt.figure()
plt.imshow(face_compressed ,  cmap="gray")
plt.show()


from sklearn import datasets
from sklearn.cluster import KMeans
from  matplotlib import pyplot as plt
import numpy as np

def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)

        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                        centers = 4,
                        cluster_std=[1.0, 2.5, 0.5, 3.0],
                        random_state=random_state)
    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

#1 zad
X=generate_data(500, 3) 
plt.figure()
plt.scatter(X[:,0], X[:,1])

plt.figure()
kmeans = KMeans(n_clusters=4, random_state=0).fit(X) 
y_predict=kmeans.fit_predict(X) 
plt.scatter(X[:,0],X[:,1],c=y_predict)

centroids = kmeans.cluster_centers_     
plt.scatter(centroids[:, 0], centroids[:, 1], 
            marker='x', color="black")
print (centroids)
plt.show()
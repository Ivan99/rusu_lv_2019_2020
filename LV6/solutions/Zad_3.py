import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
import sklearn.metrics as sm
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import PolynomialFeatures
from sklearn import preprocessing


#1Zad

def generate_data(n):
    
    #prva klasa
    n1 = int(n/2)
    x1_1 = np.random.normal(0.0, 2, (n1,1))
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1))); 
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1))
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = int(n - n/2)
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2)
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data  = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)    
    data = data[indices,:]
    
    return data
np.random.seed(242)
data_train = generate_data(200)

np.random.seed(12)
data_test = generate_data(100)

#2Zad

plt.figure()
for i in range (0,len(data_train)):
    if(data_train[i,2]) == 0:
        color="blue"
    else:
        color="black"
    plt.scatter(data_train[i,0],data_train[i,1],c=color)
plt.xlabel('x1')
plt.ylabel('x2')
plt.show()

#3Zad
model = lm.LogisticRegression()
model.fit(data_train[:,:-1], data_train[:,2])

X2=-(model.intercept_ + model.coef_[0,0]*data_train[:,0])/model.coef_[0,1] 

plt.plot(data_train[:,0],X2)
plt.show()

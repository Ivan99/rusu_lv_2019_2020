import sys

try:
    ocjena = float(input("unesi ocjenu"))
    
except ValueError:
    print("Nije broj")
    sys.exit()
    

if float(ocjena)>=0.9 and float(ocjena)<1.1:
    print("A")
elif float(ocjena)>=0.8 and float(ocjena)<0.9:
    print("B")
elif float(ocjena)>=0.7 and float(ocjena)<0.8:
    print("C")
elif float(ocjena)>=0.6 and float(ocjena)<0.7:
    print("D")
elif float(ocjena)<0.6 and float(ocjena)>0:
    print("F")
else:
    print("Broj izvan opsega")    

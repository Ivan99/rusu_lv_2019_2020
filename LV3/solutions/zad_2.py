import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('LV3\\resources\\mtcars.csv') 
#1
cylinder4 = mtcars[mtcars.cyl ==4]
cylinder6 = mtcars[mtcars.cyl ==6]
cylinder8 = mtcars[mtcars.cyl ==8]




# plt.bar(cylinder4.car, cylinder4.mpg)
# plt.bar(cylinder6.car, cylinder6.mpg)
# plt.bar(cylinder8.car, cylinder8.mpg)
# plt.title("Potrosnja automobila s 4, 6 i 8 cilindara")
# plt.legend(['4 cilindra','6 cilindara','8 cilindara'],loc=2)
# plt.xticks(rotation=90)
# index1 = np.arange(0,len(cylinder4),1)
# index2 = np.arange(0,len(cylinder6),1)
# index3 = np.arange(0,len(cylinder8),1)

# width = 0.3
# plt.show()



#2

# tezina4=[]
# tezina6=[]
# tezina8=[]
# for i in cylinder4["wt"]:
#     tezina4.append(i) 

# for i in cylinder6["wt"]:
#     tezina6.append(i)   

# for i in cylinder8["wt"]:
#     tezina8.append(i)   
# plt.figure()
# plt.boxplot([tezina4, tezina6, tezina8], positions = [4,6,8]) 
# plt.title("Tezina automobila s 4, 6 i 8 cilindara")
# plt.xlabel('Broj klipova')
# plt.ylabel('Tezina wt')
# plt.grid(axis='y',linestyle='--')

# plt.show()

#3
automatski=mtcars[(mtcars.am== 1)]
# potr_aut=[]
# for i in automatski["mpg"]:
#     potr_aut.append(i)
    
rucni=mtcars[(mtcars.am== 0)]
# potr_rucni=[]
# for i in rucni["mpg"]:
#     potr_rucni.append(i)
    
# plt.figure()
# plt.boxplot([potr_rucni, potr_aut], positions = [0,1],sym='k+')
# plt.title("Potrosnja automobila s rucnim i automatskim mjenjacem")
# plt.ylabel('mpg')
# plt.xlabel('0=rucni mjenjac, 1=automatski mjenjac')
# plt.grid(axis='y',linestyle='--')

# plt.show()

#4

ubrzanje_a=[]
snaga_a=[]
ubrzanje_r=[]
snaga_r=[]

for i in automatski["qsec"]:  
    ubrzanje_a.append(i)
    
for i in automatski["hp"]:  
    snaga_a.append(i)
    
for i in rucni["qsec"]:  
    ubrzanje_r.append(i)
    
for i in rucni["hp"]:  
    snaga_r.append(i)

plt.figure()
plt.scatter(ubrzanje_a, snaga_a, marker='^')   
plt.scatter(ubrzanje_r, snaga_r, marker='d', facecolors='none', edgecolors='r')
plt.title("Ubrzanje/Snaga automobila s rucnim i automatskim mjenjacem")
plt.ylabel('Snaga - hp')
plt.xlabel('Ubrzanje - qsec')
plt.legend(["Automatski mjenjac","Rucni mjenjac"])
plt.grid(linestyle='--')

plt.show()

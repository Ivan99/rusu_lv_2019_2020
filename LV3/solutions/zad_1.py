import pandas as pd

#1
mtcars = pd.read_csv('LV3\\resources\\mtcars.csv') 
sortedmtcars = mtcars.sort_values(by=['mpg'], ascending=True) 
print ("Najvecu potrosnji imaju:")
print (sortedmtcars['car'].head(5)) 
print ("\n")

#2 
cylinder8 = mtcars[mtcars.cyl ==8]
sortedcylinder8 = cylinder8.sort_values(by=['mpg'], ascending=False)
print ("Najmanju portosnju a da ima 8 cilindara imaju:") 
print (sortedcylinder8['car'].head(3))
print ("\n")

#3
cylinder6 = mtcars[mtcars.cyl ==6]
sred_potr = cylinder6['mpg'].mean()
print ("srednja potrosnja sa 6 cilindara:")
print (sred_potr)
print ("\n")

#4
cylinder4 = mtcars[(mtcars.cyl ==4) & (mtcars.wt>2) & (mtcars.wt<2.2)]
sred_potr = cylinder4['mpg'].mean()
print ("srednja potrosnja sa 4 cilindra mase izmedju 2000 i 2200lbs:")
print (sred_potr)
print ("\n")

#5
automatski = mtcars.groupby('am').car
print ("rucni mjenjac je 0 automatski 1:")
print (automatski.count())
print("\n")

#6
automatski = mtcars[(mtcars.am==0) &(mtcars.hp>100)]
print ("automobili sa automatskim mjenjacem i snagom od preko 100hp:")
print (automatski['car'].count())
print("\n")

#7
pound=0.45359237
a=0
print ("masa svakog automobila u kilogramima:")
for i in mtcars['wt']:
    row = i*1000
    row = row*pound
    print ("%d. auto: %.2f kg" % (a,row))
    a=a+1

